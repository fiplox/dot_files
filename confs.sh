#!/bin/sh
cp -r ./config/* ~/.config/
cp xinitrc ~/.xinitrc
cp -r bin/ ~/
cp alert.wav $HOME/.alert.wav
sudo cp fiprofile.sh /etc/profile.d/ && sudo chmod +x /etc/profile.d/fiprofile.sh
sudo cp 20-amdgpu.conf /etc/X11/xorg.conf.d/ && chmod +x /etc/X11/xorg.conf.d/20-amdgpu.conf
sudo cp tlp.conf /etc/tlp.conf
sudo cp /modprobe.d/* /etc/modprobe.d/
