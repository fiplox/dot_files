#fish_vi_key_bindings
fish_default_key_bindings

alias goex='cd ~/exercism/go/'
alias gh='cd ~/github/'
alias gge='cd ~/github/go-exercism/'
alias dot='cd ~/github/dot_files'
alias wifi-menu='nmtui'
alias up='sudo pacman -Syu'
alias c='clear'
alias cp="cp -i"              
alias clean='sudo pacman -Rsn (pacman -Qdtq)'
alias v=nvim
alias vi=nvim
alias vim=nvim
alias sv=sudoedit
alias mv='mv -i'
alias ln='ln -i'
alias rm='echo "Try again with rmt."; false'
alias rmt='trash-put'
alias untar='tar -xvf'
alias ls='lsd'
alias la='lsd -A'
alias ll='lsd -l'
alias lt='lsd --tree'
alias lla='lsd -lA'
alias cat=bat
alias gotest='golangci-lint run --enable-all'
alias update-music='mpc update && mpc clear && mpc add (mpc ls)'
alias um='sudo umount /mnt'
alias q=exit
alias gs='git status'
alias gp='git push'
alias gpll='git pull'
alias gcm='git commit -m'
alias ga='git add'
alias gpom='git pull origin master'
alias gb='git branch'
alias gck='git checkout'
alias gd='git diff'
alias gl='git log'
#alias grep='rg'
alias icat='kitty +kitten icat --align left'
alias gpp='g++ -std=c++20'

bind \cx 'fg'

set PATH $PATH /home/user/.cargo/bin
set -Ux JAVA_OPTS '-XX:+IgnoreUnrecognizedVMOptions'
set -g fish_prompt_pwd_dir_length 0
set -g theme_display_date no
set -x -U GOPATH $HOME/go
set PATH $HOME/go/bin $HOME/bin $PATH
