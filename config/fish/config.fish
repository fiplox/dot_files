#fish_vi_key_bindings
fish_default_key_bindings
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'" 
# set -x MANPAGER "nvim -c 'set ft=man' -"

alias up='sudo pacman -Syu'
alias c='clear'
alias cp="cp -i"              
alias clean='sudo pacman -Rsn (pacman -Qdtq)'
alias v=nvim
alias vi=nvim
alias vim=nvim
alias sv=sudoedit
alias mv='mv -i'
alias ln='ln -i'
alias rm='echo "Try again with rmt."; false'
alias rmt='trash-put'
alias untar='tar -xvf'
alias ls='lsd -X'
alias la='lsd -AX'
alias ll='lsd -lX'
alias lt='lsd --tree'
alias lla='lsd -lAX'
alias cat=bat
alias gotest='golangci-lint run --enable-all'
alias update-music='mpc update && mpc clear && mpc add (mpc ls)'
alias um='sudo umount /mnt'
alias q=exit
alias gs='git status'
alias gp='git push'
alias gpll='git pull'
alias gcm='git commit -m'
alias ga='git add'
alias gpom='git pull origin master'
alias gb='git branch'
alias gck='git checkout'
alias gd='git diff'
alias gl='git log'
#alias grep='rg'
alias icat='kitty +kitten icat --align left'
alias gpp='g++ -std=c++20'
alias underscore='echo -e -n "\x1b[\x34 q"'
alias p=python
alias ssh_phone='ssh -p 8022 u0_a207@192.168.0.10'
alias mutt=neomutt

bind \cx 'fg'

set PATH $PATH /home/user/.cargo/bin
set -Ux JAVA_OPTS '-XX:+IgnoreUnrecognizedVMOptions'
set -g fish_prompt_pwd_dir_length 0
set -g theme_display_date no
set -x -U GOPATH $HOME/go
set PATH $HOME/go/bin $HOME/bin $PATH
