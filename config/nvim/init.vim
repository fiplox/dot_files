" autocmds
"autocmd InsertEnter * norm zz
autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')
augroup AutoSaveFolds
  autocmd!
  " view files are about 500 bytes
  " bufleave but not bufwinleave captures closing 2nd tab
  " nested is needed by bufwrite* (if triggered via other autocmd)
  autocmd BufWinLeave,BufLeave,BufWritePost ?* nested silent! mkview!
  autocmd BufWinEnter ?* silent! loadview
augroup end

function! s:init_fern() abort
	nmap <buffer> H <Plug>(fern-action-open:split)
	nmap <buffer> V <Plug>(fern-action-open:vsplit)
	nmap <buffer> R <Plug>(fern-action-rename)
	nmap <buffer> M <Plug>(fern-action-move)
	nmap <buffer> C <Plug>(fern-action-copy)
	nmap <buffer> N <Plug>(fern-action-new-path)
	nmap <buffer> T <Plug>(fern-action-new-file)
	nmap <buffer> D <Plug>(fern-action-new-dir)
	nmap <buffer> S <Plug>(fern-action-hidden-toggle)
	nmap <buffer> dd <Plug>(fern-action-trash)
	nmap <buffer> <leader> <Plug>(fern-action-mark)
	nmap <buffer> s <Plug>(easymotion-overwin-f)
endfunction

augroup fern-custom
	autocmd! *
	autocmd FileType fern call s:init_fern()
augroup END

augroup my-glyph-palette
  autocmd! *
  autocmd FileType fern call glyph_palette#apply()
augroup END

" Lightline config
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly', 'filename', 'modified', 'text' ] ]
      \ },
      \ }

" lets
let g:rainbow_active = 1
let mapleader=","
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
"let g:webdevicons_enable_nerdtree = 1
let g:EasyMotion_do_mapping = 0 " Disable default mappings
let g:EasyMotion_smartcase = 1
let g:fern#renderer = "nerdfont"
let g:rustfmt_autosave = 1
let g:racer_cmd = "/home/user/.cargo/bin/racer"
let g:racer_experimental_completer = 1
let g:cpp_attributes_highlight = 1
let g:cpp_member_variable_highlight = 0
let g:cpp_member_highlight = 1
let g:cpp_simple_highlight = 1
"let g:cpp_no_function_highlight = 0


 " pop up menu colors
highlight Pmenu ctermbg=55 ctermfg=15
highlight PmenuSbar ctermbg=White
highlight PmenuThumb ctermbg=White


" maps
map <Leader>tt :vnew term://fish<CR> 

" nmaps
nmap ; :
nmap <C-c> :ccl <bar> :lclose<CR>
nmap <C-N><C-N> :set nu! rnu!<CR>
nmap s <Plug>(easymotion-overwin-f)
nmap <Leader>gl :runtime! syntax/gtk3.vim syntax/glib.vim<CR>
nmap <Leader>k :tabnext<CR>
nmap <Leader>j :tabprevious<CR>

" nnoremaps (normal mode maps)
nnoremap <C-Left> <C-w>h
nnoremap <C-Down> <C-w>j
nnoremap <C-Up> <C-w>k
nnoremap <C-Right> <C-w>l
nnoremap <silent> <C-Space> :nohlsearch<Bar>:echo<CR>
nnoremap <F9> za
nnoremap <F10> zd
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
nnoremap ff gg=G

" inoremaps (insert mode maps)
inoremap <PageUp> <C-x><C-o>
inoremap <F9> <C-O>za

" noremaps
noremap <silent> <C-h> :vertical resize +3<CR>
noremap <silent> <C-l> :vertical resize -3<CR>
noremap <silent> <C-k> :resize +3<CR>
noremap <silent> <C-j> :resize -3<CR>
noremap <silent> <C-f> :Fern . -drawer -toggle <CR>

" onoremaps
onoremap <F9> <C-C>za

" vnorepams
vnoremap <F9> zf

" sets
set path=.,,**h
set mouse=v
set nu rnu
"set expandtab           " expand tabs to spaces
set tabstop=4       " tab width is 4 spaces
set shiftwidth=4        " indent also with 4 spaces
"set completeopt-=preview 
set clipboard+=unnamedplus
set encoding=utf-8
set updatetime=300                      " Faster completion
set nocompatible
set smartindent
"set cinoptions+=>2s,t0
set noshowmode
set smartcase
set laststatus=2
set hlsearch
set cursorline
set splitbelow splitright
set viewoptions=folds,cursor
set sessionoptions=folds


syntax on
filetype plugin on
hi CursorLine cterm=NONE ctermbg=Black
hi Folded ctermbg=Black
"hi Function ctermfg=DarkCyan
"hi MatchParen ctermfg=Black
highlight clear SignColumn
highlight GitGutterAdd guifg=#009900 ctermfg=Green
highlight GitGutterChange guifg=#bbbb00 ctermfg=Yellow
highlight GitGutterDelete guifg=#ff2222 ctermfg=Red

""""""""""""""""""""""""""""""""""""""""""""""""""""""'''
" COC CONFIG
""""""""""""""""""""""""""""""""""""""""""""""""""""""""'
set hidden
set nobackup
set nowritebackup
"set cmdheight=2
set shortmess+=c
"set signcolumn=yes
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
"inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""'

" vim-plug 
call plug#begin()

Plug 'itchyny/lightline.vim'
"Plug 'frazrepo/vim-rainbow'
Plug 'preservim/nerdcommenter'
Plug 'lilydjwg/colorizer'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'preservim/nerdtree'
"Plug 'ryanoasis/vim-devicons'
Plug 'jiangmiao/auto-pairs'
Plug 'easymotion/vim-easymotion'
Plug 'lambdalisue/nerdfont.vim'
Plug 'lambdalisue/fern.vim'
Plug 'lambdalisue/fern-renderer-nerdfont.vim'
Plug 'lambdalisue/glyph-palette.vim'
Plug 'alx741/vim-rustfmt'
Plug 'racer-rust/vim-racer'

call plug#end()
