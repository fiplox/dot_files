" autocmds
"autocmd InsertEnter * norm zz " center screen when enter to insert mode
"autocmd FileType c,cpp,h ClangFormatAutoEnable
autocmd FileType markdown inoremap <A-i> ![](<++>){#fig:<++>}<Space><CR><CR><++><Esc>kkF]i
autocmd FileType markdown inoremap <A-a> [](<++>)<Space><++><Esc>F]i
autocmd FileType markdown nnoremap <A-1> yypVr=
autocmd FileType markdown inoremap <A-1> <Esc>yypVr=
autocmd FileType markdown nnoremap <A-2> yypVr-
autocmd FileType markdown inoremap <A-2> <Esc>yypVr-
autocmd FileType markdown inoremap <A-3> <Esc>I### <Esc>A
autocmd FileType markdown inoremap <A-4> <Esc>I#### <Esc>A
autocmd FileType markdown inoremap <A-5> <Esc>I##### <Esc>A
autocmd FileType markdown inoremap <A-u> <Esc>I+<Space><Esc>A
autocmd FileType markdown nnoremap <A-u> <Esc>I+<Space><Esc>A
autocmd FileType markdown inoremap <A-o> <Esc>I1.<Space><Esc>A
autocmd FileType markdown nnoremap <A-o> <Esc>I1.<Space><Esc>A
autocmd FileType markdown nnoremap <Leader>e I<center><Esc>A</center>

autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')
augroup AutoSaveFolds
	autocmd!
	" view files are about 500 bytes
	" bufleave but not bufwinleave captures closing 2nd tab
	" nested is needed by bufwrite* (if triggered via other autocmd)
	autocmd BufWinLeave,BufLeave,BufWritePost ?* nested silent! mkview!
	autocmd BufWinEnter ?* silent! loadview
augroup end

function! s:init_fern() abort
	nmap <buffer> H <Plug>(fern-action-open:split)
	nmap <buffer> V <Plug>(fern-action-open:vsplit)
	nmap <buffer> R <Plug>(fern-action-rename)
	nmap <buffer> M <Plug>(fern-action-move)
	nmap <buffer> C <Plug>(fern-action-copy)
	nmap <buffer> N <Plug>(fern-action-new-path)
	nmap <buffer> T <Plug>(fern-action-new-file)
	nmap <buffer> D <Plug>(fern-action-new-dir)
	nmap <buffer> S <Plug>(fern-action-hidden-toggle)
	nmap <buffer> dd <Plug>(fern-action-trash)
	nmap <buffer> <leader> <Plug>(fern-action-mark)
	nmap <buffer> s <Plug>(easymotion-overwin-f)
endfunction

augroup fern-custom
	autocmd! *
	autocmd FileType fern call s:init_fern()
augroup END

augroup my-glyph-palette
	autocmd! *
	autocmd FileType fern call glyph_palette#apply()
augroup END

"function! Formatonsave()
  "let l:formatdiff = 1
  "py3f /usr/share/clang/clang-format.py
"endfunction
"autocmd BufWritePre *.h,*.cc,*.cpp call Formatonsave()

" Lightline config
let g:lightline = {
			\ 'colorscheme': 'wombat',
			\ 'active': {
			\   'left': [ [ 'mode', 'paste' ],
			\             [ 'readonly', 'filename', 'modified', 'text' ] ]
			\ },
			\ }

" lets

let g:mkdp_auto_start = 0
let g:mkdp_browser = 'firefox'
let g:rainbow_active = 1
let mapleader=","
" GO
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
let g:go_def_mapping_enabled = 0
let g:go_highlight_structs = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_variable_declarations = 1
let g:go_highlight_variable_assignments = 1
"let g:webdevicons_enable_nerdtree = 1
" Easy motion
let g:EasyMotion_do_mapping = 0 " Disable default mappings
let g:EasyMotion_smartcase = 1
" Fern
let g:fern#renderer = "nerdfont"
"let g:rustfmt_autosave = 1
"let g:racer_cmd = "/home/user/.cargo/bin/racer"
"let g:racer_experimental_completer = 1
" C/C++
let g:cpp_attributes_highlight = 1
let g:cpp_member_variable_highlight = 0
let g:cpp_member_highlight = 1
let g:cpp_simple_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_class_scope_highlight = 1
"let g:cpp_no_function_highlight = 0
let g:clang_format#style_options = {
            \ "AccessModifierOffset" : -2,
            \ "AllowShortIfStatementsOnASingleLine" : "Never",
			\ "AllowShortLoopsOnASingleLine" : "false",
            \ "AlwaysBreakTemplateDeclarations" : "true",
            \ "Standard" : "c++20",
			\ "NamespaceIndentation" : "All",
			\ "IndentWidth" : 4,
            \ "UseTab" : "false",
            \ "BreakBeforeBraces" : "Stroustrup",
			\ "AllowShortFunctionsOnASingleLine" : "Empty",
			\ "AllowShortBlocksOnASingleLine" : "Never",
			\ "FixNamespaceComments" : "true",
			\ "PointerAlignment" : "Right",
			\ "DerivePointerAlignment" : "false"}

" pop up menu colors
highlight Pmenu ctermbg=black ctermfg=lightred
highlight PmenuSel ctermbg=darkgrey ctermfg=cyan
highlight PmenuSbar ctermbg=Black
highlight PmenuThumb ctermbg=White


" maps
map <Leader>tt :vnew term://fish<CR> 

" nmaps
nmap <C-p> <Plug>MarkdownPreviewToggle
nmap cl :ClangFormatAutoToggle<CR>
nmap ; :
nmap <C-c> :ccl <bar> :lclose<CR>
nmap <C-N><C-N> :set nu! rnu!<CR>
nmap s <Plug>(easymotion-overwin-f)
nmap <Leader>gl :runtime! syntax/gtk3.vim syntax/glib.vim<CR>
nmap <Leader>k :tabnext<CR>
nmap <Leader>j :tabprevious<CR>
nmap <Leader>s :set spell spelllang=fr<CR>
nmap <A-q> z=
nmap <C-q> :q<CR>
nm <silent> <F1> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name")
    \ . '> trans<' . synIDattr(synID(line("."),col("."),0),"name")
    \ . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name")
    \ . ">"<CR>

" nnoremaps (normal mode maps)
nnoremap <C-Left> <C-w>h
nnoremap <C-Down> <C-w>j
nnoremap <C-Up> <C-w>k
nnoremap <C-Right> <C-w>l
nnoremap <silent> <C-Space> :nohlsearch<Bar>:echo<CR>
nnoremap <F9> za
nnoremap <F10> zd
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
nnoremap ff gg=G
nnoremap <A-s> :%s///gI<Left><Left><Left><Left>

xnoremap <leader>p "_dP

" inoremaps (insert mode maps)
inoremap <PageUp> <C-x><C-o>
inoremap <F9> <C-O>za
"inoremap ;g <++>
"inoremap <leader><Tab> <Esc>/<++><Enter>"_c4l
inoremap <A-q> <Esc>z=

" noremaps
noremap <silent> <C-h> :vertical resize +3<CR>
noremap <silent> <C-l> :vertical resize -3<CR>
noremap <silent> <C-k> :resize +3<CR>
noremap <silent> <C-j> :resize -3<CR>
noremap <silent> <C-f> :Fern . -drawer -toggle <CR>
noremap <leader><Tab> <Esc>/<++><Enter>"_c4l

" onoremaps
onoremap <F9> <C-C>za

" vnorepams
vnoremap <F9> zf
vnoremap <leader><Tab> <Esc>/<++><Enter>"_c4l

" sets
set guicursor=n-v-c-sm:hor20,i-ci-ve:ver20,r-cr-o:Block
set path=.,,**h
set mouse=v
set nu rnu
"set expandtab           " expand tabs to spaces
set tabstop=4       " tab width is 4 spaces
set shiftwidth=4        " indent also with 4 spaces
"set completeopt-=preview 
set clipboard+=unnamedplus
set encoding=utf-8
set updatetime=300                      " Faster completion
set nocompatible
set smartindent
"set cinoptions+=>2s,t0
set noshowmode
set smartcase
set laststatus=2
set hlsearch
set cursorline
set splitbelow splitright
set viewoptions=folds,cursor
set sessionoptions=folds
"if has('termguicolors')
  "set termguicolors
"endif


syntax on
filetype plugin on
hi CursorLine cterm=NONE ctermbg=Black
hi Folded ctermbg=Black
"hi Function ctermfg=DarkCyan
"hi MatchParen ctermfg=White
highlight clear SignColumn
highlight GitGutterAdd guifg=#009900 ctermfg=Green
highlight GitGutterChange guifg=#bbbb00 ctermfg=Yellow
highlight GitGutterDelete guifg=#ff2222 ctermfg=Red
hi Comment ctermfg=darkgrey
"GO
"hi goParamType ctermfg=darkgreen
hi goParamName ctermfg=lightgreen
hi goFunctionCall cterm=bold ctermfg=blue
hi goBuiltins ctermfg=lightblue
highlight link JavaIdentifier NONE
""""""""""""""""""""""""""""""""""""""""""""""""""""""'''
" COC CONFIG
""""""""""""""""""""""""""""""""""""""""""""""""""""""""'
set hidden
set nobackup
set nowritebackup
"set cmdheight=2
set shortmess+=c
"set signcolumn=yes
inoremap <silent><expr> <TAB>
			\ pumvisible() ? "\<C-n>" :
			\ <SID>check_back_space() ? "\<TAB>" :
			\ coc#refresh()
"inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""'

" vim-plug 
call plug#begin()

Plug 'itchyny/lightline.vim'
Plug 'preservim/nerdcommenter'
Plug 'lilydjwg/colorizer'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jiangmiao/auto-pairs'
Plug 'easymotion/vim-easymotion'
Plug 'lambdalisue/nerdfont.vim'
Plug 'lambdalisue/fern.vim'
Plug 'lambdalisue/fern-renderer-nerdfont.vim'
Plug 'lambdalisue/glyph-palette.vim'
Plug 'alx741/vim-rustfmt'
"Plug 'racer-rust/vim-racer'
Plug 'uiiaoo/java-syntax.vim'
Plug 'rhysd/vim-clang-format'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
call plug#end()
