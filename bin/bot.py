#!/bin/env python3
from notify import notification
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

a = "Il n'existe plus de plage horaire libre pour votre demande de rendez-vous. Veuillez recommencer ultérieurement."
#This example requires Selenium WebDriver 3.13 or newer
with webdriver.Firefox() as driver:
    wait = WebDriverWait(driver, 10)
    driver.get("https://www.seine-saint-denis.gouv.fr/booking/create/9829")
    driver.fullscreen_window()
    driver.find_element(By.NAME, "condition").click()
    driver.find_element(By.NAME, "nextButton").click()
    test = driver.find_element(By.NAME, "create").get_attribute("textContent")
    # first_result = wait.until(presence_of_element_located((By.CSS_SELECTOR, "h3>div")))
    print(driver.current_url)
    print(test)
    if driver.current_url != "https://www.seine-saint-denis.gouv.fr/booking/create/9829/2" and a not in test:
        notification("YESYESYESYESYEYSEYEYSEYEYSEYES", "PREF")
    elif a in test:
        notification("NOPE", "PREF")
    else:
        notification("ERR", "PREF")
