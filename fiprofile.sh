#!/bin/sh
export TERMINAL=alacritty
export BROWSER=firefox
export EDITOR=nvim
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH="$HOME/.cargo/bin:$HOME/bin:$HOME/go/bin:$HOME/.local/bin:$HOME/.npm-global/bin:$PATH"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export WINIT_X11_SCALE_FACTOR=1
export NPM_CONFIG_PREFIX=$HOME/.npm-global
export _JAVA_AWT_WM_NONREPARENTING=1
export AWT_TOOLKIT=MToolkit
export LS_COLORS='di=38;5;33:ln=38;5;44:so=38;5;44:pi=38;5;44:bd=38;5;44:or=38;5;124:cd=38;5;172:ex=38;5;40:fi=38;5;184:no=38;5;245'
#export PATH_TO_FX=$PATH_TO_FX:/usr/lib/jvm/java-15-openjdk/lib
#export JAVA_HOME=/usr/lib/jvm/default
