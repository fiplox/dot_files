#!/bin/sh
export TERMINAL=alacritty
export BROWSER=firefox
export EDITOR=nvim
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH="$HOME/.cargo/bin:$HOME/bin:$HOME/go/bin:$HOME/.local/bin:$PATH"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export WINIT_X11_SCALE_FACTOR=1
